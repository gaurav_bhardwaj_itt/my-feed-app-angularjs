'Use Strict';
todoApp.controller('todoController',function ($scope) {
//variable declaration
$scope.imagepreview = 0;
$scope.datas = [];

//Get data from localstorage
if(localStorage.getItem('datas') != null){
    $scope.datas = JSON.parse(localStorage.getItem('datas'));
   }

//Function to convert uploaded image in base64
File.prototype.convertToBase64 = function(callback){
            var reader = new FileReader();
            reader.onload = function(e) {
                 callback(e.target.result)
            };
            reader.onerror = function(e) {
                 callback(null);
            };
            reader.readAsDataURL(this);
    };

//On Change event of choose file.
$("#logo").on('change',function(){
            var selectedFile = this.files[0];
            selectedFile.convertToBase64(function(base64){
            $scope.imageFile = base64;
                })
            });

//Get the comment and set in localstorage
$scope.writeComment = function (data) {
            data.thoughts.push(data.comment);
            localStorage.setItem('datas',JSON.stringify($scope.datas));
            data.comment = "";
            };
//To increase the like count and store in localstorage
$scope.like = function (data) {
            data.likeCount = data.likeCount+1;
            localStorage.setItem('datas',JSON.stringify($scope.datas));
            };
//To increase the counter for showing comment box
$scope.showComments = function (data) {
            if(!data.showCommentBox){
              data.showCommentBox=true;
            }
            else{
              data.showCommentBox = false;
            }
            localStorage.setItem('datas',JSON.stringify($scope.datas));
            };
//Save data in localstorage on post button click
$scope.writePost = function () {
            $scope.imagepreview = 1;
            //Store Data in json format.
            $scope.datas.push({
                data_message : $scope.message,
                likeCount : 0,
                base64image : $scope.imageFile,
                thoughts : [],
                created_at : new Date(),
                showCommentBox : false
                });
            localStorage.setItem('datas',JSON.stringify($scope.datas));
            $scope.message = ""; //blank the textbox after posting
            };
           });



